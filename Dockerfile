FROM rustlang/rust:nightly as build

WORKDIR /src
USER root
COPY . ./
RUN cargo build --release && \
    strip ./target/release/jmoon_dev

FROM tdewolff/minify as minifier

WORKDIR /src
COPY ./static/css .
RUN mkdir -p /css && \
    for f in *.css; do minify "$f" -o "/css/$f"; done

FROM gcr.io/distroless/cc:latest
WORKDIR /app

COPY --from=build /src/target/release/jmoon_dev ./
COPY --from=minifier /css/ ./static/css
COPY ./static/files  ./static/files
COPY ./static/fonts   ./static/fonts
COPY ./static/html    ./static/html
COPY ./static/images  ./static/images
COPY ./templates ./templates
EXPOSE 8000
ENV ROCKET_ADDRESS=0.0.0.0
ENTRYPOINT ["/app/jmoon_dev"]
