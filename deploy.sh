#!/bin/bash
set -o errexit
if [ ! -d "$1" ]; then
	printf "Usage: %s <path to deployment directory>\n" "$0"
	exit 1
fi

deploy="$1"

cargo build --release
./minify_css.sh

sudo systemctl stop jmoon_dev
sudo cp -ar ./target/release/jmoon_dev ./static ./templates  "$deploy"
sudo mkdir -p "${deploy}/youtube-dl-outputs"
sudo chown nginx:nginx "${deploy}/youtube-dl-outputs"
sudo cp -a ./jmoon_dev.service /usr/lib/systemd/system/
sudo cp -a ./jmoon_dev.conf /etc/
sudo chown nginx:nginx /etc/jmoon_dev.conf
sudo systemctl daemon-reload
sudo systemctl start jmoon_dev
rm -f "${deploy}/static/css/"{normalize.css,styles.css,big.css,small.css}
