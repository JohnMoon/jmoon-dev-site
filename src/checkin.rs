use std::process::Command;
use rocket::form::Form;
use rocket_dyn_templates::Template;
use std::{thread, time};

const SW_CHECKIN_IMAGE: &str = "pyro2927/southwestcheckin:latest";

/// Template context passed to Tera template HTML file
#[derive(Serialize)]
pub struct CheckinContext {
    info: String,
    found_flight: bool
}

/// The CheckinForm sends the user's info
#[derive(FromForm, Debug)]
pub struct CheckinForm {
    conf_num: String,
    fname: String,
    lname: String
}

pub fn run_checkin(form: &CheckinForm) -> CheckinContext {
    // Set unique ID for container
    let cont_id = format!("{}_{}_{}", form.conf_num, form.fname, form.lname);

    let _ = Command::new("docker")
        .args(vec!["rm".to_string(), "-f".to_string(), cont_id.clone()])
        .status();

    let mut args = vec!["run".to_string(), "--rm".to_string(), "-d".to_string(), "--name".to_string(), cont_id.clone()];
    args.append(&mut vec![SW_CHECKIN_IMAGE.to_string(), form.conf_num.clone(), form.fname.clone(), form.lname.clone()]);

    let status = Command::new("docker")
        .args(&args)
        .status();

    if status.is_err() || !status.unwrap().success() {
        return CheckinContext {
            found_flight: false,
            info: "Failed to execute the checkin service!".to_string()
        };
    }

    let mut found_flight = false;
    let mut info = "".to_string();

    // Give the up to 10 seconds to find the flights
    let max_time_ms = 10_000;
    let mut t = 0;
    let step_ms = 1000;
    let mut last_iter = false;
    let post_find_buffer_ms = 2_000;

    while t < max_time_ms {
        found_flight = false;
        info = "".to_string();

        let output = Command::new("docker")
            .args(vec!["logs".to_string(), cont_id.clone()])
            .output();

        if let Ok(out) = output {
            if !out.status.success() {
                return CheckinContext {
                    found_flight: false,
                    info: "Failed to parse checkin service output!".to_string()
                };
            }

            let stdout = String::from_utf8_lossy(&out.stdout);

            if last_iter {
                found_flight = true;
                info = stdout.to_string();
                break;
            }

            for l in stdout.lines() {
                if l.contains("Flight information found") {
                    last_iter = true;
                }
                info = format!("{}\n{}", info, l);
            }

            if last_iter {
                thread::sleep(time::Duration::from_millis(post_find_buffer_ms));
                continue;
            }

        } else {
            info = "Failed to parse checkin service output!".to_string();
        }

        thread::sleep(time::Duration::from_millis(step_ms));
        t += step_ms;
    }

    if t >= max_time_ms {
        info = "Timed out trying to find flight information!".to_string();
    }

    CheckinContext { info, found_flight }
}

#[get("/checkin")]
pub fn checkin_get() -> Template {
    let ctx = CheckinContext {
        info: "".to_string(),
        found_flight: false,
    };

    Template::render("checkin", &ctx)
}

/// Route to deal with the downloader post data
#[post("/checkin", data = "<form>")]
pub fn checkin_post(form: Form<CheckinForm>) -> Template {
    Template::render("checkin", run_checkin(&form))
}
