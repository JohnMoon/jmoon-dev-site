extern crate zip;
extern crate walkdir;

use std::path::{Path, PathBuf};
use rocket::form::Form;
use rocket::fs::NamedFile;
use rocket_dyn_templates::Template;
use std::fs;
use walkdir::WalkDir;
use std::io::prelude::*;

use youtube_dl::{YoutubeDl, YoutubeDlOutput};
use id3::{Tag, Version, TagLike};

/// Template context passed to Tera template HTML file
#[derive(Serialize)]
pub struct DownloaderContext {
    path: String,
    fname: String,
    fsize: String,
    error: String
}

#[derive(Debug, FromFormField, Clone, PartialEq)]
pub enum Format {
    Mp3, Wav, M4a, Aav, Mp4, Webm
}

impl Format {
    pub fn ext(&self) -> &str {
        match self {
            Format::Mp3 => "mp3",
            Format::Wav => "wav",
            Format::M4a => "m4a",
            Format::Aav => "aav",
            Format::Mp4 => "mp4",
            Format::Webm => "webm"
        }
    }

    pub fn is_video(&self) -> bool {
        matches!(self, Format::Mp4 | Format::Webm)
    }

    pub fn is_audio(&self) -> bool {
        !self.is_video()
    }
}

/// The QuoteForm sends the user's guess
#[derive(FromForm, Debug)]
pub struct DownloaderForm {
    url: String,
    playlist: bool,
    format: Format,
    title: String,
    artist: String,
    album: String,
    year: String,
}

pub fn zip_dir(dir: &str) -> Result<String, String> {
    let basename = Path::new(dir).file_stem().unwrap();
    let zip_path = format!("{}.zip", dir);
    let zip_file = match std::fs::File::create(&zip_path) {
        Ok(z) => z,
        Err(s) => return Err(s.to_string()),
    };

    let mut zip = zip::ZipWriter::new(zip_file);
    if zip.add_directory(basename.to_str().unwrap(), zip::write::FileOptions::default()).is_err() {
        return Err("Error adding directory to zip archive".to_string());
    }

    // Add each file in the playlist directory to the archive
    let mut buf = Vec::new();
    for entry in WalkDir::new(dir).into_iter().filter_map(|e| e.ok()) {
        if ! entry.path().is_file() {
            continue;
        }
        if let Ok(filename) = entry.path().strip_prefix(Path::new("./youtube-dl-outputs")) {
            if let Some(filename) = filename.to_str() {
                println!("{}", filename);
                if zip.start_file(filename, zip::write::FileOptions::default()).is_ok() {
                    if let Ok(mut f) = std::fs::File::open(entry.path()) {
                        if f.read_to_end(&mut buf).is_ok() {
                            if zip.write(&buf).is_err() {
                                return Err("Failed to flush output zip archive".to_string());
                            }
                        } else {
                            return Err("Failed to read individual file".to_string());
                        }
                        buf.clear();
                    } else {
                        return Err("Failed to open individual file".to_string());
                    }
                } else {
                    return Err("Failed to start output file".to_string());
                }
            } else {
                return Err("Failed to convert file name to string".to_string());
            }
        } else {
            return Err("Failed to strip output prefix from file".to_string());
        }
    }

    Ok(zip_path)
}

pub fn download_and_convert(form: &DownloaderForm) -> Result<String, String> {

    let ytdl_output_fmt = format!("./youtube-dl-outputs/{}", {
        if form.playlist {
            "%(playlist_title)s/%(title)s.%(ext)s"
        } else {
            "%(title)s.%(ext)s"
        }
    });

    let output = YoutubeDl::new(&form.url)
        .socket_timeout("15")
        .format(form.format.ext())
        .extract_audio(form.format.is_audio())
        .extra_arg("--restrict-filenames")
        .extra_arg("-o")
        .extra_arg(&ytdl_output_fmt)
        .run();

    let output = match output {
        Ok(out) => out,
        Err(e) => return Err(format!("failed to download media: {}", e))
    };

    let basename = {
        match output {
            YoutubeDlOutput::Playlist(p) => {
                p.title.unwrap()
            }
            YoutubeDlOutput::SingleVideo(v) => {
                format!("{}.{}", v.title, v.ext.unwrap())
            }
        }
    };

    let path = format!("./youtube-dl-outputs/{}", basename);

    // Add Mp3 tag if it's an Mp3 file
    if form.format == Format::Mp3 && !form.playlist {
        add_tag(&path, form);
    }

    // If it's a playlist, zip up the output folder
    if form.playlist {
        zip_dir(&basename)
    } else {
        Ok(basename)
    }
}

pub fn get_context_from_path(path: &str) -> Result<DownloaderContext, String> {
    if let Some(fname) = Path::new(&path).file_name() {
        if let Some(fname) = fname.to_str() {
            if let Ok(meta) = fs::metadata(&path) {
                let fsize_kbytes = meta.len() / 1_000;
                Ok(DownloaderContext {
                    path: path.to_string(),
                    fname: fname.to_string(),
                    fsize: format!("{} KB", fsize_kbytes),
                    error: "".to_string()
                })
            } else {
                Err("Failed to get filesize from output".to_string())
            }
        } else {
            Err("Failed to get output filename".to_string())
        }
    } else {
        Err("Failed to get output filename".to_string())
    }

}

pub fn add_tag(file: &str, form: &DownloaderForm) {
    let mut tag = Tag::new();
    tag.set_artist(form.artist.clone());
    tag.set_album(form.album.clone());
    tag.set_title(form.title.clone());

    // In case it's an invalid year, just skip it
    if let Ok(year) = form.year.clone().parse::<i32>() {
        tag.set_year(year);
    }

    // If we fail to write to the file, no big deal
    if tag.write_to_path(file, Version::Id3v24).is_err() {
        println!("error writing MP3 tags to file");
    }
}

#[get("/downloader")]
pub fn downloader_get() -> Template {
    let ctx = DownloaderContext {
        path: "".to_string(),
        fname: "".to_string(),
        fsize: "".to_string(),
        error: "".to_string()
    };

    Template::render("downloader", &ctx)
}

/// Route to deal with the downloader post data
#[post("/downloader", data = "<form>")]
pub fn downloader_post(form: Form<DownloaderForm>) -> Template {
    let mut error = "".to_string();
    let mut ctx: Option<DownloaderContext> = {
        match download_and_convert(&form) {
            Ok(p) => {
                match get_context_from_path(&p) {
                    Ok(c) => Some(c),
                    Err(s) => {
                        error = s;
                        None
                    }
                }
            },
            Err(e) => {
                error = e;
                None
            }
        }
    };

    // In an error case, report the error to the user
    if ctx.is_none() {
        ctx = Some(DownloaderContext {
            path: "".to_string(),
            fname: "".to_string(),
            fsize: "".to_string(),
            error
        });
    }

    Template::render("downloader", &ctx)
}

/// Route to load static files
#[get("/youtube-dl-outputs/<file..>")]
pub async fn download_file(file: PathBuf) -> Option<NamedFile> {
    NamedFile::open(Path::new("youtube-dl-outputs/").join(file)).await.ok()
}
