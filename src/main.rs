#![feature(proc_macro_hygiene, decl_macro, plugin)]

#[macro_use] extern crate rocket;
#[macro_use] extern crate serde_derive;
#[macro_use] extern crate magic_crypt;
extern crate serde_json;
extern crate url;
extern crate regex;

use std::path::{Path, PathBuf};
use std::env;
use magic_crypt::MagicCryptTrait;
use rocket::State;
use rocket::fs::NamedFile;
use rocket_dyn_templates::Template;
use regex::RegexBuilder;
use rand::Rng;

mod downloader;
mod checkin;
mod catchers;

const QUOTES: &str = include_str!("quotes/quotes.txt");
const BACKUP_QUOTE: &str = include_str!("quotes/backup_quote.txt");

/// Contact info struct for managed state
struct ContactInfo {
    email: String,
    phone: String
}

/// Template context passed to Tera template HTML file
#[derive(Serialize)]
struct TemplateContext {
    quote: String,
    encrypted_email: String,
    encrypted_phone: String,
}

/// The QuoteForm sends the user's guess
#[derive(FromForm, Serialize)]
struct QuoteForm {
    user_guess: String
}

/// Generate a new quote using the "fortune" command (or a bacup quote if that fails)
fn get_new_quote() -> String {
    let re = RegexBuilder::new(r"^%$")
        .multi_line(true)
        .build()
        .unwrap();
    let quotes: Vec<&str> = re.split(QUOTES).collect();
    println!("Got {} quotes\n", quotes.len());
    let rand_index = rand::thread_rng().gen_range(0..quotes.len());
    quotes.get(rand_index).unwrap_or(&BACKUP_QUOTE).to_string()
}

/// Route deals with the default GET path to the webroot
#[get("/")]
fn render_get(contact_info: &State<ContactInfo>) -> Template {
    let quote = get_new_quote();
    let third_word = quote.split_whitespace().nth(2).unwrap().to_string().to_uppercase();

    let mc = new_magic_crypt!(third_word, 256);
    let encrypted_email = mc.encrypt_str_to_base64(contact_info.email.clone());
    let encrypted_phone = mc.encrypt_str_to_base64(contact_info.phone.clone());

    let ctx = TemplateContext {
        quote,
        encrypted_email,
        encrypted_phone
    };

    Template::render("index", &ctx)
}

/// Route to load drinks page
#[get("/drinks")]
async fn drinks() -> Option<NamedFile> {
    NamedFile::open(Path::new("static/html/drinks.html")).await.ok()
}

/// Route to load static files
#[get("/static/<file..>")]
async fn static_files(file: PathBuf) -> Option<NamedFile> {
    NamedFile::open(Path::new("static/").join(file)).await.ok()
}

/// Favicon route
#[get("/favicon.ico")]
async fn favicon() -> Option<NamedFile> {
    NamedFile::open(Path::new("static/images/favicon.png")).await.ok()
}

#[get("/robots.txt")]
async fn robots() -> Option<NamedFile> {
    NamedFile::open(Path::new("static/files/robots.txt")).await.ok()
}

/// Main rocket ignition
#[launch]
fn rocket() -> _ {
    let email = env::var("DEV_EMAIL").expect("DEV_EMAIL must be set in the environment");
    let phone = env::var("DEV_PHONE").expect("DEV_PHONE must be set in the environment");

    let contact_info = ContactInfo { email, phone };

    rocket::build()
        .manage(contact_info)
        .mount("/", routes![render_get,
                            drinks,
                            static_files,
                            favicon,
                            robots,
                            downloader::downloader_get,
                            downloader::downloader_post,
                            downloader::download_file,
                            checkin::checkin_get,
                            checkin::checkin_post])
        .register("/", catchers![catchers::internal_error,
                                 catchers::unproc_ent,
                                 catchers::bad_request,
                                 catchers::not_found])
        .attach(Template::fairing())
}

#[test]
fn test_downloader_input() {
    use std::io::BufRead;
    let client = rocket::local::Client::new(rocket()).expect("valid rocket instance");

    // Pass malformed data and get a bad request
    let response = client.post("/downloader")
        .body("url=https%3A%2F%2Fwww.youtube.com%2Fwatch%3Fv%3DdQw4w9WgXcQ&bad_field=asdf")
        .header(rocket::http::ContentType::Form)
        .dispatch();
    assert_eq!(response.status(), rocket::http::Status::UnprocessableEntity);

    // Check that a Rick roll downloads
    let response = client.post("/downloader")
        .body("url=https%3A%2F%2Fwww.youtube.com%2Fwatch%3Fv%3DdQw4w9WgXcQ&format=MP3&title=&artist=&album=&year=")
        .header(rocket::http::ContentType::Form)
        .dispatch();
    assert_eq!(response.status(), rocket::http::Status::Ok);
    assert!(Path::new("./youtube-dl-outputs/Rick_Astley_-_Never_Gonna_Give_You_Up_Video.mp3").exists());

    // Throw attack strings at the form
    let input_f = std::fs::File::open(TEST)
        .expect("error - input not found");
    let input = std::io::BufReader::new(&input_f);
    for attack in input.lines().map(Result::unwrap) {
        // All of these attacks should return a 200, 400, or 422 status
        let response = client.post("/downloader")
            .body(attack)
            .header(rocket::http::ContentType::Form)
            .dispatch();

        assert!(response.status() == rocket::http::Status::Ok ||
                response.status() == rocket::http::Status::BadRequest ||
                response.status() == rocket::http::Status::UnprocessableEntity);
    }
}

#[test]
fn test_checkin_input() {
    use std::io::BufRead;
    let client = rocket::local::Client::new(rocket()).expect("valid rocket instance");

    // Pass malformed data and get a bad request
    let response = client.post("/checkin")
        .body("conf_num=123ABC&fname=John&lname=Smith&bad_field=asdf")
        .header(rocket::http::ContentType::Form)
        .dispatch();
    assert_eq!(response.status(), rocket::http::Status::UnprocessableEntity);

    // Check that just conf_num, fname, and lname works
    let response = client.post("/checkin")
        .body("conf_num=123ABC&fname=John&lname=Smith")
        .header(rocket::http::ContentType::Form)
        .dispatch();
    assert_eq!(response.status(), rocket::http::Status::Ok);

    // Check that conf_num, fname, lname, and email work
    let response = client.post("/checkin")
        .body("conf_num=123ABC&fname=John&lname=Smith&email=john@smith.com")
        .header(rocket::http::ContentType::Form)
        .dispatch();
    assert_eq!(response.status(), rocket::http::Status::Ok);

    // Throw attack strings at the form
    //let input_f = std::fs::File::open(TEST)
    //    .expect("error - input not found");
    //let input = std::io::BufReader::new(&input_f);
    //for attack in input.lines().map(Result::unwrap) {
    //    // All of these attacks should return a 200, 400, or 422 status
    //    let response = client.post("/checkin")
    //        .body(attack)
    //        .header(rocket::http::ContentType::Form)
    //        .dispatch();

    //    assert!(response.status() == rocket::http::Status::Ok ||
    //            response.status() == rocket::http::Status::BadRequest ||
    //            response.status() == rocket::http::Status::UnprocessableEntity);
    //}
}
